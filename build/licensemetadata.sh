find . -name "*.jpg" | while read file
do
exiftool -by-line="Hugo Gruson" -Copyright="© Hugo Gruson ; \
Creative Commons CC-by-sa 4.0 \
(https://creativecommons.org/licenses/by-sa/4.0/)" \
$file
echo $file 'processed'
done
