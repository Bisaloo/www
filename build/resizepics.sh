#! /bin/sh

# This was written to overcome the fact that mogrify doesn't support the
# specification of the pattern for output filename.
# Parameters from https://stackoverflow.com/a/7262050
# Depends on imagemagick.

JPGTORESIZE=`find $@ -name \*.jpg_original`
COUNT=`echo $(echo $JPGTORESIZE | wc -w)`
SIZE='512x512'

for i in $JPGTORESIZE; do
  convert -strip \
          -interlace Plane \
          -gaussian-blur 0.05 \
          -quality 85% \
          -resize $SIZE \
          $i $(basename $i .jpg_original)_$SIZE.jpg
  echo $(basename $i .jpg_original)' done.'
done

echo ''
echo $COUNT' files converted to '$SIZE'.'
