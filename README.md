# How to build your website

## Get started: General guides
- Stack exchange documentation: [HTML](https://stackoverflow.com/documentation/html/topics), [CSS](https://stackoverflow.com/documentation/css/topics) and [SVG](https://stackoverflow.com/documentation/svg/topics)
- [Mozilla developper blog](https://developer.mozilla.org/)
- [W3Schools](http://www.w3schools.com/)
- Effect of every CSS property illustrated with an exemple : [CSSreference](http://cssreference.io/)

## Pictures
- [ ] For privacy and speed improvements, remove metadata from all pictures
- [ ] Optimize and compress `.svg`
- [ ] Compess JPEG both losslessly and lossily using [build/resizepics.sh](build/resizepics.sh)
- [ ] Add multiple sizes of the same picture with `srcset` to ensure your pictures always look good whatever the display size or dpp is.

## Icons
#### Existing icon sets
- [ ] [Material.io](https://material.io/icons/) by Google

## Apache config
- [ ] Leverage browser caching
- [ ] Compress content. 

  *NB: in spite of what you may read in some places, it is not recommended to compress JPEG images. Those formats are already compressed and if done right, you will get no additional gain form gzipping them. Read more on [stackexchange](https://webmasters.stackexchange.com/questions/8382/gzipped-images-is-it-worth).*

## Search engine optimization
- [ ] [Google](https://developers.google.com/speed/pagespeed/insights/)

## Validation & Quality control
#### Compliance to standards
- [ ] **HTML:** [here by w3](https://validator.w3.org/nu/?doc=https%3A%2F%2Fwww.normalesup.org%2F~hgruson%2F) and [here](https://html5.validator.nu/?doc=https%3A%2F%2Fwww.normalesup.org%2F~hgruson%2F)
- [ ] **CSS:** [here by w3](https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fwww.normalesup.org%2F~hgruson%2F&profile=css3&usermedium=all&warning=1&vextwarning=&lang=en)
- [ ] **SVG:**

#### Beautify your source code
- [ ] Wrap lines to 80 characters width
- [ ] Lint your code to have consistent and standard style.
  * [CSS](http://csslint.net/)

#### Check brower compatibility
- [ ] Are your features supported by all browsers with [do I use](http://www.doiuse.com/?url=https%3A%2F%2Fwww.normalesup.org%2F~hgruson%2F&browsers=&css=)

#### General security check
- [ ] https://github.com/FallibleInc/security-guide-for-developers

#### HTTPS
- [ ] Replace all `http://` references by protocol agnostic links

#### Speed
- [ ] Measure page loading times and identify bottlenecks with [webpagetest](https://www.webpagetest.org/)
- [ ] Analysis and tips by [yahoo](http://yslow.org/)

#### HTTP headers security
- https://securityheaders.io/?q=https%3A%2F%2Fwww.normalesup.org%2F~hgruson/
- https://httpsecurityreport.com/?report=https://www.normalesup.org/~hgruson/
