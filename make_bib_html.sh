#!/usr/bin/env bash

wget 'https://api.archives-ouvertes.fr/search/?q=authIdHal_s:hugo-gruson&wt=bibtex' -O publist.bib

~/bibtex2html-1.99-linux/bib2bib --remove NOTE publist.bib | ~/bibtex2html-1.99-linux/bibtex2html -doi-prefix "https://doi.org/" -d -r -nodoc -no-keywords -no-abstract -nobibsource -o publist 

sed -i "s#Hugo Gruson#<u>Hugo Gruson</u>#g" publist.html
sed -i 's#\.pdf</a>#<i class="far fa-file-pdf"></i> pdf</a>#g' publist.html
sed -i 's#>http<#>https<#g' publist.html


